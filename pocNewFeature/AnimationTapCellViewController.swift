//
//  ViewController.swift
//  pocNewFeature
//
//  Created by Diego Crozare on 05/05/19.
//  Copyright © 2019 Diego Crozare. All rights reserved.
//

import UIKit
import Foundation

class AnimationTapCellViewController: UIViewController {
    
    let array = ["cell1", "cell2", "cell3"]
    var testConstraint: CGFloat = 0
    
    @IBOutlet weak var constraint: NSLayoutConstraint!
    @IBOutlet weak var viewBackGround: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.register(CellTestTableViewCell.self)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
 
        let contentView = Bundle.main.loadNibNamed("InternalScreenViewController", owner: self, options: nil)?.first as! InternalScreenViewController
      //  contentView.delegate = AnimationTapCellViewController()
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        container.addSubview(contentView)
        
        self.viewBackGround.isUserInteractionEnabled = false
        testConstraint = constraint.constant
  
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        animation(interaction: false, close: true, containet: 672.5)
    }
    
    @IBAction func testeAction(_ sender: Any) {
        print("deu certo")
    }
    
    private func animation(interaction: Bool, close: Bool, containet: CGFloat = 140) {
        UIView.animate(withDuration: 1) {
           var alpha: CGFloat = 1
            if close {
                alpha = 0.4
            }
            self.viewBackGround.isUserInteractionEnabled = interaction
            self.container.center.y = containet
            self.viewBackGround.alpha = alpha
            self.view.layoutIfNeeded()
        }
    }
}

//EXTENSION

//MARK: - UITableViewDataSource

extension AnimationTapCellViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellTestTableViewCell.identifier, for: indexPath) as! CellTestTableViewCell
        cell.setup(array[indexPath.row])
        
        return cell
    }
}

//MARK: - UITableViewDelegate

extension AnimationTapCellViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        animation(interaction: true, close: false)
    }
}
