//
//  CellTestTableViewCell.swift
//  pocNewFeature
//
//  Created by Diego Crozare on 05/05/19.
//  Copyright © 2019 Diego Crozare. All rights reserved.
//

import UIKit

class CellTestTableViewCell: UITableViewCell, NibProtocol, Reusable {

    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var labelText: UILabel!

    func setup(_ text: String) {
        labelText.text = text
        viewStatus.backgroundColor = UIColor.red
    }
    
}
