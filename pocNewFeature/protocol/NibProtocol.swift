//
//  NibProtocol.swift
//  pocNewFeature
//
//  Created by Diego Crozare on 07/05/19.
//  Copyright © 2019 Diego Crozare. All rights reserved.
//

import Foundation
import UIKit

protocol NibProtocol {
    static var nibName: String { get }
    static var nib: UINib { get }
}

protocol Reusable {
    static var identifier: String { get }
}

extension Reusable {
    static var identifier: String {
        return String(describing: self)
    }
}

extension NibProtocol where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: nibName, bundle: nil)
    }
}

extension UITableView {
    func register<T: UITableViewCell>(_ : T.Type) where T: Reusable, T: NibProtocol {
        register(T.nib, forCellReuseIdentifier: T.identifier)
    }
}
